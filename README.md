# My personal page https://johuet.ch

This webpage aims to present me and showcase the work I've done.

### It was made possible thanks to several third parties:

- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
- The styling is made from [Bootstrap5](https://getbootstrap.com/)
- Hosted by [Firebase hosting](https://firebase.google.com/docs/hosting)
