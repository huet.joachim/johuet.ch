import React from 'react';

export default function Footer() {
    return (
        <footer className="container text-center mt-auto">
            <p className="lh-1" style={{fontSize: '0.8rem', fontFamily: "Coolvetica, Helvetica"}}>© Made by <a href="https://johuet.ch" className="text-reset text-decoration-none"> <span className="fw-bold" style={{color: "#e64a19"}}>Joachim.</span></a></p>
        </footer>
    );
}